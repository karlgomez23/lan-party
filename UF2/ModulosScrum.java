
import java.util.*;
import java.util.Scanner;


public class ModulosScrum {
    //Variables fijas
//    public static String[] nombresParticipantes = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
//
//    public static String[] pcParticipantes = {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""};
    public static String[] nombresParticipantes = {"", "", "Andrea Baldomero", "Esteban Leotta", "Lucas Leal", "Julieta Calvo", "Sebastian Alarcon",
            "Barbara Neuman", "Nicolas Stasiak", "German Marchetta", "Ivan Cappello", "Jesús Guzman", "Jessica Perez", "Carolina Galineres",
            "Matias Gutierrez", "Cristian Torres"};

    public static String[] pcParticipantes = {"", "", "Torre DELL", "Portatil DELL", "Portatil Huawaei", "Torre HP", "Torre DELL", "Portatil DELL",
            "Portatil Huawaei", "Torre HP", "Torre DELL", "Portatil DELL", "Portatil Huawaei", "Torre HP", "Torre DELL", "Portatil DELL"};


    public static Scanner sc = new Scanner(System.in);

    public static int[] usersNumber = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

    public static int[] game1 = {};
    public static int[] game2 = {};

    public static int[] puntuacion = new int[8];

    public static int[] equipoGanador = new int[8];

    public static String primeraMatch ="a";
    //Rellenar datos usuarios

    public static void pedirDatos() {
        int cantidadGente = 0;
        boolean correcto = false;
        for (; cantidadGente < 2; cantidadGente++) {
            System.out.println("\nPARTICIPANTE " + (cantidadGente + 1));
            while (!correcto) {
                System.out.print("Edad: ");
                if (compararEdad() >= 12) {
                    correcto = true;
                    introducirDatos(cantidadGente);
                } else {
                    System.out.println("No puedes participar");
                }
            }
            correcto = false;
        }
    }

    public static int compararEdad() {
        int edad = sc.nextInt();
        return edad;
    }

    public static void introducirDatos(int cantidadGente) {
        System.out.println("\nPARTICIPANTE " + (cantidadGente + 1));
        System.out.print("Datos del pc: ");
        sc.nextLine();
        pcParticipantes[cantidadGente] = sc.nextLine();
        System.out.print("Nombre y apellido: ");
        nombresParticipantes[cantidadGente] = sc.nextLine();
        sc.nextLine();
        System.out.println("Lugar asignado es: " + (cantidadGente + 1));
    }

    //Imprimir datos Usuarios
    public static void imprimirParticipantes () {
        int lugar = 1;
        for (int i = 0; i < nombresParticipantes.length; i++) {
            System.out.println("##############################\n" +
                    "#       PARTICIPANTE " + (i + 1)+ "\n" +
                    "##############################\n"+
                    "#   Nombre: " + nombresParticipantes[i]+"\n" +
                    "#   PC: " + pcParticipantes[i] + "\n" +
                    "#   Lugar asignado: " + lugar + "\n"+
                    "##############################");
            lugar++;
        }
    }

    //Control de la partida

    //creacion de la partida
    public static void creacionPartida() {
        System.out.print("Usuarios: ");
        System.out.println(Arrays.toString(usersNumber)
                .replace("[", "")
                .replace("]", "")
                .replace(",", ""));

        Random rand = new Random();

        for (int i = 0; i < 1; i++) {
            int randomIndextoSwap = rand.nextInt(usersNumber.length);
            int temp = usersNumber[randomIndextoSwap];
            usersNumber[randomIndextoSwap] = usersNumber[i];
            usersNumber[i] = temp;
            Boolean terminado = true;

            if (usersNumber.length > 16) {
                terminado = true;
            } else {
                if (usersNumber.length == 16) {
                    int split = usersNumber.length;
                    terminado = false;
                    game1 = new int[(split + 1) / 2];
                    game2 = new int[split - game1.length];
                    for (int j = 0; j < split; j++) {
                        if (j < game1.length) {
                            game1[j] = usersNumber[j];
                        } else {
                            game2[j - game1.length] = usersNumber[j];
                        }
                    }
                    System.out.print("Equipo A: ");
                    System.out.println(Arrays.toString(game1)
                            .replace("[", "")
                            .replace("]", "")
                            .replace(",", ""));
                    System.out.print("Equipo B: ");
                    System.out.println(Arrays.toString(game2)
                            .replace("[", "")
                            .replace("]", "")
                            .replace(",", ""));
                }
            }
        }
        indiqarGanadorPartida();
    }
    //Indicar cual es su equipo
    public static void indicarEquipo() {
        primeraMatch = Arrays.toString(game1) + " vs " + Arrays.toString(game2);
        System.out.println(" ");
        System.out.println("INDIQUE SU EQUIPO A CONTINUACION PARA SABER CONTRA QUIEN LE TOCARA (equipoA/equipoB): ");

    }

    //indica que equipo gano
    public static void indiqarGanadorPartida(){


        boolean correcto = false;
        while (!correcto) {
            indicarEquipo();
            String equip = sc.nextLine();
            if (equip.equals("equipoA") || equip.equals("equipoB")) {
                System.out.println(primeraMatch);
                System.out.println("");
                System.out.println("INDIQUE EL GANADOR DE LAN PARTY: ");
                String ganadores = sc.nextLine();
                if (Objects.equals(ganadores, "equipoA")) {
                    String ganadorEquipo = Arrays.toString(game1);
                    for (int x = 0; x < ganadorEquipo.length(); x++) {
                        System.out.print(ganadorEquipo.charAt(x));
                    }
                    for (int l = 0; l < 8; l++) {
                        equipoGanador[l] = game1[l];
                    }
                } else {
                    String ganadorEquipo = Arrays.toString(game2);
                    for (int x = 0; x < ganadorEquipo.length(); x++) {
                        System.out.print(ganadorEquipo.charAt(x));
                    }
                    for (int l = 0; l < 8; l++) {
                        equipoGanador[l] = game2[l];
                    }
                }
                System.out.println();
                correcto = true;
            } else {
                System.out.println("TU EQUIPO NO ESTA REGISTRADO");
            }
        }
    }

    //Puntuaciones
    public static void puntuacionGanador() {

        //Insercion y muestra de puntucacion

        Arrays.sort(equipoGanador);

        System.out.println("\nINDIQUEN SU PUNTUACIÓN A CONTINUACIÓN");
        for (int x = 0; x < 8; x++) {
            System.out.print(equipoGanador[x] + ":" + nombresParticipantes[equipoGanador[x] - 1] + " = ");
            puntuacion[x] = sc.nextInt();
        }
        ordenadorEquipoGanador();
    }

    public static void ordenadorEquipoGanador() {

        for (int c = 0; c < 8 - 1; c++) {
            for (int a = 0; a < 8 - c - 1; a++) {
                if (puntuacion[a] > puntuacion[a + 1]) {
                    int temporal = puntuacion[a];
                    int temporalEquipo = equipoGanador[a];
                    puntuacion[a] = puntuacion[a + 1];
                    equipoGanador[a] = equipoGanador[a + 1];
                    puntuacion[a + 1] = temporal;
                    equipoGanador[a + 1] = temporalEquipo;
                }
            }
        }
        clasidicacionGanador();
    }

    public static void clasidicacionGanador() {

        System.out.println("\nCLASIFICACIÓN DEL EQUIPO GANADOR:");

        int puesto = 1;
        for (int j = 7; j >= 0; j--) {
            System.out.println(puesto + " - " + equipoGanador[j] + ":" + nombresParticipantes[equipoGanador[j] - 1] + " = " + puntuacion[j]);
            puesto++;
        }
    }



}


