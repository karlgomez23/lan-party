import java.util.Comparator;
import java.lang.Integer;

public class Jugador extends Persona{
    private String pc;
    private String alias;
    private int userNumbre;
    private int puntuacion;

    public Jugador(String nom, int edad, String pc, String alias, int userNumbre) {
        super(nom, edad);
        this.pc = pc;
        this.alias = alias;
        this.userNumbre = userNumbre;
        this.puntuacion = 0;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getUserNumbre() {
        return userNumbre;
    }

    public void setUserNumbre(int userNumbre) {
        this.userNumbre = userNumbre;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public static Comparator<Jugador> jugadorComparatorPuntuacion = new Comparator<Jugador>() {
        public int compare(Jugador v1, Jugador v2){

            int jugadorPuntuacion1 = v1.getPuntuacion();
            int jugadorPuntuacion2 = v2.getPuntuacion();

            return Integer.compare(jugadorPuntuacion2, jugadorPuntuacion1);
        }
    };

    @Override
    public String toString() {
        return "##############################\n" +
                "#       PARTICIPANTE " + userNumbre + "\n" +
                "##############################\n" +
                super.toString() + "\n" +
                "#   Alias: " + alias + "\n" +
                "#   PC: " + pc + "\n" +
                "##############################";
    }
}
