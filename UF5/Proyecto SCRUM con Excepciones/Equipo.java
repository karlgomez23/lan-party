import java.util.ArrayList;

public class Equipo implements Torneo {
    private String nomEquipo;
    private ArrayList<Jugador> jugadors;

    public Equipo(String nomEquipo, ArrayList<Jugador>jugadors) {
        this.nomEquipo = nomEquipo;
        this.jugadors = jugadors;
    }

    public String getNomEquipo() {
        return nomEquipo;
    }

    public void setNomEquipo(String nomEquipo) {
        this.nomEquipo = nomEquipo;
    }

    public ArrayList<Jugador> getJugadors() {
        return jugadors;
    }

    public void setJugadors(ArrayList<Jugador> jugadors) {
        this.jugadors = jugadors;
    }

    @Override
    public boolean compararSiExiste(String equipo) {
        return equipo.equals(getNomEquipo());
    }

    @Override
    public String toString() {
        return "Nombre del equipo = " + nomEquipo;
    }
}
