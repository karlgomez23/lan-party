import java.io.*;
import java.util.*;
import java.util.Scanner;

import static java.util.Arrays.sort;

public class ModulosScrum {
    private ArrayList<Equipo> equipos;

    public ModulosScrum(){
        equipos = new ArrayList<>();
    }

    public static void main(String[] args) {
        ModulosScrum modulosScrum = new ModulosScrum();

        modulosScrum.indicarCantidadEquipos();
        modulosScrum.imprimirParticipantes();
        modulosScrum.indicarGanadorPartida();
    }

    public static int[] equipoGanador = new int[8];
    //Rellenar datos usuarios

    public void indicarCantidadEquipos() {
        Scanner sc = new Scanner(System.in);
        boolean correcto = false;
        while (!correcto) {
            try {
                System.out.println("Indique la cantidad de equipos que desea tener");
                int cantidadEquipos = sc.nextInt();
                System.out.println("De cuantos jugadores?");
                int cantidadJugadoresEquipo = sc.nextInt();
                if((cantidadJugadoresEquipo % cantidadEquipos) == 0 && cantidadEquipos != 1){
                    correcto = true;
                    pedirDatos(cantidadEquipos, cantidadJugadoresEquipo);
                } else {
                    System.out.println("Al menos 2 jugadores por equipo");
                }
            } catch (InputMismatchException err) {
                System.out.println("Debe ingresar un número entero");
                sc.next();
            }
        }
    }
    public void pedirDatos(int cantidadEquipos,int cantidadJugadoresEquipo) {
        Scanner sc = new Scanner(System.in);

        int cantidadEquiposActuales = 0;
        while(cantidadEquiposActuales < cantidadEquipos) {
            System.out.println("Indique nombre del equipo");
            equipos.add(new Equipo(sc.next(), new ArrayList<>()));
            for(int i = 1; i <= cantidadJugadoresEquipo; i++) {
                System.out.println("\nPARTICIPANTE " + i);
                int edad;
                do {
                    System.out.print("Edad: ");
                    try {
                        edad = sc.nextInt();
                        if (edad >= 12) {
                            System.out.println("Puedes optar a premio");
                        } else {
                            System.out.println("No puedes optar a premio");
                        }
                    } catch (InputMismatchException err) {
                        System.out.println("Debe ingresar un número entero");
                        sc.next();
                        edad = -1;
                    }
                } while(edad < 1);
                introducirDatos(cantidadEquiposActuales, edad, i);
            }
            cantidadEquiposActuales++;
        }
    }


    public void introducirDatos(int equiposActual, int edad,int usuario) {
        Scanner sc = new Scanner(System.in);
        //aqui se puede hacer otra excepcion por si hay un emocionado que pone otro valor en el
        //alias, nombre y pc
        System.out.print("Alias: ");
         String alias = sc.next();
        System.out.print("Nombre y apellido: ");
        sc.nextLine();
        String nom = sc.nextLine();
        System.out.print("Nombre del pc: ");
        String pc = sc.next();

        equipos.get(equiposActual).getJugadors().add(new Jugador(nom,edad,pc,alias,usuario));
    }

    //Imprimir datos Usuarios
    public void imprimirParticipantes() {
        for (Equipo equipo : equipos) {
            System.out.println(equipo);
            for (int a = 0; a < equipo.getJugadors().size(); a++) {
                System.out.println(equipo.getJugadors().get(a));
            }
        }
    }

    //Control de la partida
    //-----------------------------------------------------------------------------------
    //Indicar cual es su equipo
    public void imprimirEquipos() {
        System.out.println("INDIQUE EL EQUIPO GANADOR DE LA PARTIDA ");
        for(int i = 0; i<equipos.size();i++){
            System.out.println(equipos.get(i));
        }

    }

    //indica que equipo gano
    public void indicarGanadorPartida() {
        Scanner sc = new Scanner(System.in);

        boolean correcto = false;
        boolean equipoNoExisteError = false;
        while (!correcto) {
            imprimirEquipos();
            String equip = sc.next();
            for(int i = 0; i < equipos.size(); i++) {
                try {
                    if (equipos.get(i).compararSiExiste(equip)) {
                        System.out.println("Equipo registrado con éxito");
                        puntuacionGanador(i);
                        correcto = true;
                    }else if (!equipoNoExisteError){
                        System.err.println("El equipo no existe, vuelve a introducirlo");
                        equipoNoExisteError = true;
                    }
                } catch (IndexOutOfBoundsException err) {
                    System.err.println("Index out of bounce");
                }
            }
        }
    }

    // --------------------------------------------------------------------------------

    //Puntuaciones
    public void puntuacionGanador(int equipo) {
        Scanner sc = new Scanner(System.in);

        //Insercion y muestra de puntuacion
        ArrayList<Jugador> equipoGanador = equipos.get(equipo).getJugadors();
        System.out.println(equipos.get(equipo).getNomEquipo()+" es el ganador");
        System.out.println("INDIQUEN SU PUNTUACIÓN A CONTINUACIÓN");
        for (int i = 0; i < equipoGanador.size(); i++) {
            System.out.print(equipoGanador.get(i).getNom() + ":" + equipoGanador.get(i).getUserNumbre() + " = ");
            try {
                int puntuacion = sc.nextInt();
                equipoGanador.get(i).setPuntuacion(puntuacion);
            } catch (InputMismatchException err) {
                System.out.println("Ingrese una puntuación válida para " + equipoGanador.get(i).getNom() + ":" + equipoGanador.get(i).getUserNumbre());
                sc.next();
                i--;
            }
        }
        ordenadorEquipoGanador(equipoGanador);
        clasidicacionGanador(equipo);
    }

    public void ordenadorEquipoGanador(ArrayList<Jugador> equipoGanador) {
        Collections.sort(equipoGanador,Jugador.jugadorComparatorPuntuacion);

    }

    public void clasidicacionGanador(int equipo) {

        Equipo equipoGanadorFinal = equipos.get(equipo);
        System.out.println("CLASIFICACIÓN DEL EQUIPO GANADOR:"+ equipoGanadorFinal.getNomEquipo());

        int puesto = 1;
        for (int i = 0; i <equipoGanadorFinal.getJugadors().size(); i++) {
            System.out.println(puesto + ".   "+equipoGanadorFinal.getJugadors().get(i).getUserNumbre()+"-"+
                    equipoGanadorFinal.getJugadors().get(i).getAlias() + ":"  + " = " +
                    equipoGanadorFinal.getJugadors().get(i).getPuntuacion());
            puesto++;
        }
    }
}