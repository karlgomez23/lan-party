import java.sql.SQLException;
import java.util.List;

public interface HistorialDAO {
    public int create(Historial historial) throws SQLException;
    public List<Historial> getHistorials() throws SQLException;


}
