import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JugadorDAOImplementacio implements JugadorDAO {
    static Connection con = Connexio.getConnection();

    public int create(Jugador jugador) throws SQLException {
        String query = ("INSERT INTO Jugador(pc, alias, userNumber,puntuacion,id_equipo)VALUES(?,?,?,?)");
        PreparedStatement ps = con.prepareStatement(query);
        ps.setString(1, jugador.getPc());
        ps.setString(2,jugador.getAlias());
        ps.setInt(3,jugador.getUserNumber());
        ps.setInt(4,jugador.getPuntuacion());
        ps.setInt(5,jugador.getId_equipo());
        ResultSet rs = ps.getGeneratedKeys();
        rs.next(); // Sabem que només n'hi ha una
        int n = rs.getInt(1);
        return n;
    }
    public Jugador read (int jugadorId) throws SQLException{
        String query = "select * from Jugador where id_jugador= ?";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setInt(1, jugadorId);
        Jugador jugador = new Jugador();
        ResultSet rs = ps.executeQuery();
        boolean check = false;

        while (rs.next()){
            check=true;
            jugador.setId_jugador(rs.getInt("id"));
            jugador.setPc(rs.getString("pc"));
            jugador.setAlias(rs.getString("alias"));
            jugador.setUserName(rs.getInt("userNumber"));
            jugador.setPuntuacion(rs.getInt("puntuacion"));
            jugador.setId_equipo(rs.getInt("id_Equipo"));
        }
        if (check == true) {
            return jugador;
        }
        else
            return null;

    }

    @Override
    public void update(Jugador jugador) throws SQLException {
        String query = "UPDATE Jugador SET  id= ?, pc = ?, alias = ?, userNumber = ?, id_equipo, puntuacion = ? WHERE InvoiceLineId = ?";
        try {
            PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1,jugador.getPc());
            ps.setString(2, jugador.getAlias());
            ps.setInt(3, jugador.getUserNumber());
            ps.setInt(4, jugador.getPuntuacion());
            ps.setInt(5, jugador.getId_equipo());
            ps.executeUpdate();
            System.out.println("Update successful");
        } catch (Exception e) {
            System.out.println("Error");
        }
        System.out.println("Exit");
    }


    @Override
    public void delete(int jugadorId) throws SQLException {
        String query = "DELETE FROM Jugador WHERE id_jugador = ?";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setInt(1, jugadorId);
        ps.executeUpdate();
    }

    @Override
    public List<Jugador> getJugadors() throws SQLException {
        String query = "SELECT * FROM Jugadors";
        PreparedStatement ps = con.prepareStatement(query);
        Jugador jugador = new Jugador();
        ResultSet rs = ps.executeQuery();
        List<Jugador> ls= new ArrayList<>();
        while (rs.next()) {
            jugador.setId_jugador(rs.getInt("id_jugador"));
            jugador.setPc(rs.getString("pc"));
            jugador.setAlias(rs.getString("alias"));
            jugador.setId_jugador(rs.getInt("userNumber"));
            jugador.setPuntuacion(rs.getInt("puntuacion"));
            jugador.setId_equipo(rs.getInt("id_equipo"));
            ls.add(jugador);
        }
        return ls;
    }
}
