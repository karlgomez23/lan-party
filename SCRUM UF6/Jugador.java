import java.util.Comparator;
import java.lang.Integer;

public class Jugador extends Persona{
    private int id_jugador;
    private String pc;
    private String alias;
    private int userNumber;
    private int id_equipo;
    private int puntuacion = 0;

    public Jugador(String nom, int edad, String pc, String alias, int userNumber) {
        super(nom, edad);
        this.pc = pc;
        this.alias = alias;
        this.userNumber = userNumber;
    }

    public Jugador(int id_jugador,String nom, int edad, String pc, String alias, int userNumber,int puntuacion,int id_equipo) {
        super(nom, edad);
        this.id_jugador = id_jugador;
        this.pc = pc;
        this.alias = alias;
        this.userNumber = userNumber;
        this.id_equipo = id_equipo;
        this.puntuacion = puntuacion;
    }

    public Jugador(String pc, String alias, int userNumber,int id_equipo, int puntuacion) {

        this.pc = pc;
        this.alias = alias;
        this.userNumber = userNumber;
        this.id_equipo = id_equipo;
        this.puntuacion = puntuacion;
    }


    public Jugador() {
        super();
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getUserNumber() {
        return userNumber;
    }

    public void setUserName(int userNumber) {
        this.userNumber = userNumber;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public static Comparator<Jugador> jugadorComparatorPuntuacion = new Comparator<Jugador>() {
        public int compare(Jugador v1, Jugador v2){

            int jugadorPuntuacion1 = v1.getPuntuacion();
            int jugadorPuntuacion2 = v2.getPuntuacion();

            return Integer.compare(jugadorPuntuacion2, jugadorPuntuacion1);
        }
    };
    public int getId_jugador() {
        return id_jugador;
    }

    public void setId_jugador(int id_jugador) {
        this.id_jugador = id_jugador;
    }

    public int getId_equipo() {
        return id_equipo;
    }

    public void setId_equipo(int id_equipo) {
        this.id_equipo = id_equipo;
    }

    @Override
    public String toString() {
        return "Jugador{" +
                "jugadorId=" + id_jugador +
                ", pc='" + pc + '\'' +
                ", alias='" + alias + '\'' +
                ", userNumber=" + userNumber +
                ", id_Equipo=" + id_equipo +
                ", puntuacion=" + puntuacion +
                '}';
    }
}
