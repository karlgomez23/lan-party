import java.sql.SQLException;
import java.util.List;

public interface EquipoDAO {
    public int create(Equipo equipo) throws SQLException;
    public Equipo read(int equipoId) throws SQLException;
    public List<Equipo> getEquipos() throws SQLException;
}
