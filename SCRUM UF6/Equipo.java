import java.sql.Array;
import java.util.ArrayList;

public class Equipo implements Torneo {
    private String nomEquipo;
    private ArrayList<Jugador> jugadores;
    private int idEquipo;

    public Equipo(String nomEquipo, ArrayList<Jugador> jugadores) {
        this.nomEquipo = nomEquipo;
        this.jugadores = jugadores;

    }

    public Equipo(String nomEquipo, int idEquipo) {
        this.nomEquipo = nomEquipo;
        this.idEquipo = idEquipo;
    }
    public Equipo(String nomEquipo) {
        this.nomEquipo = nomEquipo;
    }

    public Equipo() {
    }

    public String getNomEquipo() {
        return nomEquipo;
    }

    public void setNomEquipo(String nomEquipo) {
        this.nomEquipo = nomEquipo;
    }

    @Override
    public boolean compararSiExiste(String equipo) {
        return equipo.equals(getNomEquipo());
    }

    public ArrayList<Jugador> getJugadors() {
        return jugadores;
    }

    public void setJugadores(ArrayList<Jugador> jugadores) {
        this.jugadores = jugadores;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    @Override
    public String toString() {
        return "Equipo{" +
                "nomEquipo='" + nomEquipo + '\'' +
                ", idEquipo=" + idEquipo +
                '}';
    }
}
