public class Historial {
    // create historial
    // get historial que den todos

    private int id_Equipo;
    private int id_Jugador;
    private Equipo equipo;
    private Jugador jugador;

    public Historial(int id_Equipo, int id_Jugador) {
        this.id_Equipo = id_Equipo;
        this.id_Jugador = id_Jugador;
    }
    public Historial(Equipo equipo, Jugador jugador){
        this.equipo = equipo;
        this.jugador = jugador;
    }

    public Historial() {
    }

    public int getId_Equipo() {
        return id_Equipo;
    }

    public int getId_Jugador() {
        return id_Jugador;
    }

    public void setId_Jugador(int id_Jugador) {
        this.id_Jugador = id_Jugador;
    }

    public void setId_Equipo(int id_Equipo) {
        this.id_Equipo = id_Equipo;
    }


    @Override
    public String toString() {
        return "historial{" +
                ", id_Equipo=" + id_Equipo +
                ", id_Jugador='" + id_Jugador + '\'' +
                '}';
    }

}
