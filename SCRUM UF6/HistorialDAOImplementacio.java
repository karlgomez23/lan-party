import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HistorialDAOImplementacio implements HistorialDAO{

    static Connection con = Connexio.getConnection();

    @Override
    public int create(Historial historial) throws SQLException {
        String query = "INSERT INTO Historial(id_equipo, id_jugador) VALUES (?, ?)";
        PreparedStatement ps = con.prepareStatement(query);
        ps.setInt(2,historial.getId_Equipo());
        ps.setInt(3, historial.getId_Jugador());

        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        rs.next();
        int n = rs.getInt(1);
        return n;
    }

    @Override
    public List<Historial> getHistorials() throws SQLException {
        String query = "SELECT Equipos.id_equipo, Equipos.nomEquipo, Jugador.id_jugador, Jugador.pc, Jugador.alias, Jugador.puntuacion FROM" +
                " Equipos INNER JOIN Jugador ON Equipos.id_equipo = Jugador.id_jugador";
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List<Historial> ls = new ArrayList();

        while (rs.next()){
            Equipo equipo = new Equipo();
            Jugador jugador = new Jugador();
            equipo.setIdEquipo(rs.getInt("id_equipo"));
            equipo.setNomEquipo(rs.getString("nomEquipo"));
            jugador.setId_jugador(rs.getInt("id_jugador"));
            jugador.setPc(rs.getString("pc"));
            jugador.setAlias(rs.getString("alias"));
            jugador.setPuntuacion(rs.getInt("puntuacion"));
            Historial historial= new Historial(equipo,jugador);
            ls.add(historial);
        }
        return ls;
    }

}
